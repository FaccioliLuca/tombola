import java.util.Arrays;

public class Schede {
	private int[] numeri; // numeri contenuti in questa scheda
	final private boolean[] segnati; // true se il numero corrispondente è segnato
	
	public Schede() {
		segnati = new boolean[15]; //default tutti false
		inizializzaCartella();
	}

	// Restituzione della vincita più alta
	public Vincita vincitaMax() {
		// Registrazione dei numeri estratti in ogni riga 
		final int[] righe = new int[3];
		for (int i=0; i<15; i++) {
			if (segnati[i])
				righe[(int)(i/5)] ++;
		}

		// Controlla tombola
		if (righe[0] + righe[1] + righe[2] == 15) 
			return Vincita.Tombola;
		
		// Calcola la riga con più numeri segnati
		final int v = Math.max(righe[0], Math.max(righe[1], righe[2]));
		switch(v) {
			case 2:   // due numeri estratti 
				return Vincita.Ambo;
			case 3:   // tre numeri estratti 
				return Vincita.Terna;
			case 4:   // quattro numeri estratti 
				return Vincita.Quaterna;
			case 5:   // cinque numeri estratti 
				return Vincita.Cinquina;
			default:  // zero o un numero estratto 
				return null;
		}
	}

	// estrazione dei numeri 
	public boolean numeroEstratto(final int numero) {
		// cerca il numero tra quelli presenti nella scheda, e se presente lo segna estratto
		final int indice = Utility.indexOf(numero, numeri, Amministratore.NUM_MAX_SCHEDA);
		if (indice >= 0) {
			segnati[indice] = true;
			return true;
		}
		return false;
	}
	
	// Posiziona i numeri nella scheda
	private void inizializzaCartella() {
		numeri = new int[15]; 
		
		/*
		 Regole per i 15 numeri casuali:
		 1. non si ripetono i numeri 
		 2. max 2 numeri con la stessa decina
		 */
		
		final int[] decine = new int[10]; //indica quanti numeri per ogni decina
		for (int i=0; i<15; i++) {
			// Genera un numero casuale tra 1 e 90
			final int n = Utility.generaCasuale(1, 90);
			// Decina attuale
			final int d = n==90? 8 : n/10; //il 90 va nella colonna degli 80
			/*
			 Ripeto il calcolo finchè il numero generato è disponibile, quindi 
			 se non ha già due numeri della stessa decina oppure non è già stato trovato
			*/
			if (decine[d] >= 2 || Utility.indexOf(n, numeri, i) >= 0) {
				i--;
				continue;
			} else {
				numeri[i] = n;
				decine[d]++;
			}
		}

		// Ordina il vettore finale
		Arrays.sort(numeri);

		// Permuta per ottenere le righe finali (un elemento ogni tre nel vettore ordinato)
		int tmp = numeri[1];
		numeri[1] = numeri[3];
		numeri[3] = numeri[9];
		numeri[9] = numeri[13];
		numeri[13] = numeri[11];
		numeri[11] = numeri[5];
		numeri[5] = numeri[2];
		numeri[2] = numeri[6];
		numeri[6] = numeri[4];
		numeri[4] = numeri[12];
		numeri[12] = numeri[8];
		numeri[8] = numeri[10];
		numeri[10] = numeri[5];
		numeri[5] = tmp;
		
		// Scambia (in verticale) i numeri della stessa colonna se non sono in ordine tra loro
		for (int i=0; i<15; i++) {
			final int n = numeri[i];
			final int d = n/10;
			for (int j=i; j<15; j++) {
				final int n2 = numeri[j];
				final int d2 = n2/10;
				// d==d2: stessa colonna, n>n2 ordine invertito
				if (d == d2 && n>n2) { 
					final int temp = numeri[i];
					numeri[i] = numeri[j];
					numeri[j] = temp;
				}
			}
		}
	}
	
	// Rappresentazione testuale
	public void stampa() {
		final String spacer = "   ";
		for (int r=0; r<3; r++) {
			String output = "";
			int d = 0;
			for (int c=0; c<5; c++) {
				int index = r*5+c;
				int num = numeri[index];

				// spazi per i numeri mancanti 
				int _d = (int)((double)num/10.0);
				// il 90 viene inserito nella colonna degli 80
				if (num == 90) 
					_d = 8;
				for (int i=1; i<_d-d+(c==0?1:0); i++)
					output += spacer+"   ";
				d = _d;

				// stampa numero
				output += String.format("%s%02d%c", spacer, num, (segnati[index]?'#':' '));
			}

			Utility.info(output);
		}
	}

}