import java.util.Scanner;

public class Prova {

	public static void main(String[] args) {
		Amministratore g = new Amministratore();
		/*
		 Scanner scan = new Scanner(System.in);
		 System.out.println("Ciao, inserisci il numero dei giocatori: ");
		 int NUM_GIOC = scan.nextInt();
		*/
		
		/*
		 aggiunge NUM_GIOC al gestore g. La partita inizia 
		 non appena ci sono abbastanza giocatori
		*/
		
		for (int i=0; i<g.NUM_GIOC; i++) {
			
			Scanner sc = new Scanner(System.in);
			System.out.println("Inserisci nome e cognome:");
			String nomeCognome = sc.nextLine();
			g.iscrivi(new Giocatore("Giocatore "+nomeCognome));
		}
	}
	
}