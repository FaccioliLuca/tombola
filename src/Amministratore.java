import java.util.ArrayDeque;
import java.util.Scanner;

public class Amministratore {
	
	public final static float  COSTO_SCHEDA        = 20.00f;     
	
	
	// Costanti non modificabili
	public final static int    NUM_MAX_SCHEDA    = 15;
	public final static int    RIGHE_SCHEDA  = 3;
	
	static 
	Scanner scan = new Scanner(System.in);
	static int NUM_GIOC = scan.nextInt();

	private ArrayDeque<Giocatore> coda;
	
	
	public Amministratore() {
		coda = new ArrayDeque<>();
	
	}
	
	
	public void iscrivi(Giocatore g) {
		
			
		
		// controllo se il giocatore non sia già in coda
		if (!coda.contains(g)) {
			Utility.info("Aggiunto giocatore alla coda: "+g.getNome());
			coda.addLast(g);
		}

		// raggiunto il numero minimo la partita si avvia automaticamente
		if (coda.size() >= NUM_GIOC) {
			Utility.info("È stato raggiunta la soglia di "+NUM_GIOC+" giocatori in coda. Avvio la partita");
			iniziaPartita();
		}
	}
	
		
	private void iniziaPartita() {
		// LISTA GIOCATORI
		
		final Giocatore[] giocatori = new Giocatore[NUM_GIOC];
		for (int i=0; i<NUM_GIOC; i++) {
			Giocatore g = coda.removeFirst();
			g.nuovaCartella();
			giocatori[i] = g;
		}
		
		// PARTITA
		final Game p = new Game(giocatori);
		p.avviaCiclo();
		
		// FINE
		Utility.info("RIEPILOGO");
		for (Giocatore g: giocatori) {
			g.stampa();
		}
	}
	
}
