import java.util.ArrayList;

public class Game {
	final private Giocatore[] giocatori;
	final private Estrazione tab;
	private Vincita ultimaVincita;

	
	final private static float montepremi = Amministratore.NUM_GIOC * Amministratore.COSTO_SCHEDA;
	
	public Game(final Giocatore[] g) {
		giocatori = g;
		tab = new Estrazione();
		ultimaVincita = null;
		Utility.info("Creata nuova partita");
	}
	
	// Calcola vincita
	public static float calcolaVincita(Vincita v) {
		// suddivisione dei montepremi in base alla vincita
		switch (v) {
			case Ambo:
				return montepremi/15*1;
			case Terna:
				return montepremi/15*2;
			case Quaterna:
				return montepremi/15*3;
			case Cinquina:
				return montepremi/15*4;
			case Tombola:
				return montepremi/15*5;
			default:
				return 0;
		}
	}
	
	public void avviaCiclo() {
		// ciclo infinito, termina solo quando finisce la partita
		while(true) { 
			final int n = tab.estraiNumero();
			Utility.info("Estratto numero: "+n);

			// controllo se il numero è presente in qualche cartella
			final ArrayList<Giocatore> vincitori = new ArrayList<>();
			for (Giocatore g: giocatori) {
				final Schede c = g.getCartella();
				if (c.numeroEstratto(n)) {
					Utility.info("La cartella di "+g.getNome()+" contiene il numero!");
					final Vincita v = c.vincitaMax();
					if (v == Vincita.next(ultimaVincita)) {
						// se il giocatore ha vinto si aggiunge alla lista dei vincitori per dividere il premio
						vincitori.add(g);
						Utility.info("La cartella di "+g.getNome()+" vince: "+v.name());
						c.stampa();
					}
				}
			}
			
			// suddivisione della vincita
			if (vincitori.size() > 0) {
				ultimaVincita = Vincita.next(ultimaVincita);
				float importo = calcolaVincita(ultimaVincita) / vincitori.size();
				Utility.info("Divido la vincita ("+ultimaVincita.name()+") tra "+vincitori.size()+" giocatori: ciascuno "+importo);
				for (Giocatore g: vincitori) {
					g.setVincita(ultimaVincita);
					g.aggiungiSommaVinta(importo);
				}
			}
			
			if (ultimaVincita == Vincita.Tombola) {
				Utility.info("Tombola uscita, partita finita!");
				return;
			}
		}
	}
}