

  // Memoria.

  // Faccioli Luca; Forino Annachiara; Giacomelli Eleonora.
 
public class Memoria 
{
    protected static final int LEN = 100;  // dimensione predefinita dell'array (COSTANTE)
    protected static Punto[] elementiP = new Punto[LEN]; // array degli elementi in memoria
    protected static int dimensionep=0;
    
    
    
     
     // Questo metodo mostra i valori presenti in memoria.
      
     // @param: nessuno esplicito.
     // @return: i valori che sono presenti in memoria.
     
    public static Punto[] getMemoriaP(){
        return elementiP;
    }
    
    
     
     //topPunto:restituisce il valore dell'ultimo elemento inserito nella pila.
      
     // @param: nessuno esplicito (la memoria implicitamente).
     // @return: l'ultimo elemento inserito.
     
    public static Punto topPunto()
    {
        return elementiP[dimensionep-1]; 
    }
    
    
      
     // pushPunto: aggiunge un elemento alla memoria.
     
     // @param: nessuno esplicito (la memoria implicitamente); dimensione < LEN; e definito.
     // @return: nessuno.
  
    public static void pushPunto(Punto e)
    {
        if(Punto.fullP()==true){
        enflateP();
       }
    
       elementiP[dimensionep++] = e; // Prima aggiunge e e poi incrementa dimensione di uno
        return;
    }
    
     //
     // enflate: ridimensiona la memoria, raddoppiandone la capacità.
     
     // @param: nessuno
     // @return: nessuno
     
   public static void enflateP(){
        int i=0;
        Punto[] elementi1=new Punto[2*LEN];
        for(i=0;i<dimensionep;i++){
        elementi1[i]=elementiP[i];
        }
        return;
    }

    
     
     // toString: crea una stringa che stampa gli elementi presenti in memoria.
     
     // @param: nessuno esplicito (la memoria implicitamente).
     // @return: string rappresentante i valori della memoria.
     
    public static String toStringP()
    {
        int i;
        String s = "[ ";
        for (i = 0; i < dimensionep-1; i++){
            s = s + elementiP[i].getX() + ", " + elementiP[i].getY() + "; ";
        }
        s = s+elementiP[i].getX() + ", " + elementiP[i].getY() + "]";
        return s;
    }
    
    
     
     // empty: controlla se la pila è vuota
      
     // @param: nessuno esplicito (la memoria implicitamente).
     // @return: vero se è vuota, falso altrimenti.
     
    public boolean emptyP()
    {
        return dimensionep == 0;
    }
    
   
     // pop: rimuove l'ultimo elemento inserito nella memoria.
     
     // @param: nessuno esplicito (la memoria implicitamente); la dimensione deve essere maggiore di 0.
     // @return: nessuno.
     
    public static void popP()
    {
        dimensionep = dimensionep-1;
        return;
    }
    
   
     // full: controlla se la pila è piena.
      
     // @param: nessuno esplicito (la memoria implicitamente); la dimensione deve essere maggiore di 0.
     // @return: Vero se piena, falso se non lo è.
     
    public static boolean fullP(){
        boolean a=false;
        if (dimensionep==LEN)
           a=true; 
        return a;   
    }
  
}