
  //Punto. 
 
  //Autori:  Faccioli Luca; Forino Annachiara; Giacomelli Eleonora.
 

public class Punto extends Memoria
{
    //VARIABILI D'ISTANZA
    
    public double asc, ord;
    
    //METODI COSTRUTTORI
    
   
     // @param   a numero reale
     //@param   b numero reale
     //@return  nessuno
    

    public Punto(double a, double b)
    {
        asc = a;
        ord = b;
    }
    
    
  //Nuovo punto
    
    public Punto punto(){
        return new Punto(this.getX(), this.getY());
    }
    
    
      
      
     // @param nessuno
     //@return     ascissa del punto
     
    public double getX()
    {
        return asc;
    }

    
     
      
     // @param nessuno
     // @return     ordinata del punto
     
    public double getY()
    {
        return ord;
    }
    
 
     // Stampa il punto creato.
      
     // @param nessuno
     // @return     ordinata del punto
     
    public String toString()
    {   
        String s ="(" + asc +" , "+ord+")";
        return s;
    }
    
     //modifica ascissa
     
     // @param   x   nuovo valore della coordinata
     // @return     nulla
     
    public void setX(double x)
    {
        asc = x;
        return;
    }
    
    //Modifica ordinata
      
     // @param   y   nuovo valore della coordinata
     // @return     nulla
     
    public void setY(double y)
    {
        ord = y;
        return;
    }
    
    
     
     // Calcola la distanza tra due punti
     
     // @param      p    un punto
     // @return     distanza
     
    public double distanza(Punto p)
    {
        double x = p.asc-asc;
        double y = p.ord-ord;
        double d = Math.sqrt(x*x+y*y);
        return d;
    }       
}