 

 // Interprete.

 //Autori: Faccioli Luca; Forino Annachiara; Giacomelli Eleonora.
 

import java.util.Scanner;
public class Interprete extends Memoria
{
    static Scanner tastiera2 = new Scanner(System.in);
    static Scanner tastiera = new Scanner(System.in); 
    static Scanner keyboard = new Scanner(System.in);
    
     /** Attraverso l'inserimento di un carattere esegue l'operazione relativa.
     
     @param: dle (stringa che contiene la linea inserita da input).
     @return: tkn (token).
     */
    
    public static Token decodifica(String dle){
        Token tkn;
        char ch = dle.charAt(0);
        switch (ch) {
            case '1': tkn = Token.Punto; break;
            case '3': tkn = Token.disP; break;
            case '2': tkn = Token.aggPunto; break;
            case 'M': tkn = Token.menu; break;
            case '4': tkn = Token.exit; break;
            default : tkn = Token.error; break;
            
        }
        return tkn;
    }
    
   
     // Inizializza il menù delle operazioni possibili.
 
    public static void menu () {
        System.out.println("(1) - Esercizio sul calcolo della distanza.");
        System.out.println("(4) - Concludi il programma.");
    }
        
    //Identifica ciò che si vuole fare con i punti
    
    public static void menu_punto(){
        System.out.println("(2) Aggiungi punti.");
        System.out.println("(3) Calcola la distanza fra due punti");
        System.out.println("(M) Torna al menu principale per arrestare il programma.");
    }
    
    
    public static void eseguiPunto(String dle,  Token tkn){
        Token second_tkn = tkn;
             switch (second_tkn) {
                case aggPunto:
                    System.out.println("Inserisci ascissa e ordinata ");
                         double x=tastiera2.nextDouble();
                         double y=tastiera2.nextDouble();
                         Punto A= new Punto(x,y);
                         pushPunto(A);
                    break;
                case disP:
                    if (dimensionep>1){
                        Punto a = elementiP[dimensionep-1];
                        Punto b = elementiP[dimensionep-2];
                        System.out.println("La distanza è : " + a.distanza(b)); //calcola la distanza dei due punti dati
                    }
                    else {
                        System.out.println("Non ci sono abbastanza punti in memoria!");
                    }
                    break;
                case menu : 
                    break;
                
                default: errore();
            }
        return ;
    }
  
     //Esegue la richiesta contenuta nella linea 
   
    public static void esegui(String dle,  Token tkn){
        
        switch (tkn) {
            case Punto:         
                menu_punto();
                Token stato = Token.notdef;
                String linea;
                linea = tastiera.nextLine();
                while (stato!=Token.menu){
                    stato = decodifica(linea);
                    switch (stato) {
                        case notdef:    
                                        stato = Token.exit; break;
                        case error:    errore(); stato = Token.exit; break;
                        default:        eseguiPunto(linea, stato);
                    }
                    if (stato!=Token.menu) {
                        menu_punto();
                        linea = tastiera.nextLine();
                        
                    }
                }
                break;
         
             
           
            }
        
        return;
    }
    
    
   //Eventuali errori
    public static void errore()
    {
        System.out.println("Si è verificato un errore!");
        return;
    }
    
    
  //Gestione dell'interprete dei comandi
    
    public static void DistanzaPunto()
    {
        Token stato = Token.notdef;
        String linea;
        menu();
        linea = tastiera.nextLine();
        while (stato!=Token.exit){
            stato = decodifica(linea);
            switch (stato) {
                    case exit: 
                    case notdef:    
                                    stato = Token.exit; break;
                    case error:    errore(); stato = Token.exit; break;
                    default:        esegui(linea, stato);
                }
            if (stato!=Token.exit) {
                menu();
                linea = tastiera.nextLine();
            }
        }
        System.out.println("Fine");
        return;
    }
        
    
    //Info
    
    public static void info()
    {
        System.out.println("Programmazione Matematica Applicata 2018/19\n");
        System.out.println("Progetto di Programmazione\n");
        System.out.println("IDEATORI:\n");
        System.out.println("Faccioli Luca - VR422019");
        System.out.println("Forino Annachiara- VR422000");
        System.out.println("Giacomelli Eleonora - VR422153\n");
        return;
    }

    //Main 
    
    public static void main(String[] args){
        info();
        DistanzaPunto();
        return;
    }
}

